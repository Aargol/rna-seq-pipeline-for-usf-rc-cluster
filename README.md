# README #

### About this repository ###
This repository contains a Python script that outputs a Bash script given an input file. The input file contains the locations of a directory containing RNA-seq reads, a GTF file, and a genome index in FASTA format. To run the program the user would edit the input file and then type `python slurm_generator.py` to create several files and submit them to the USF Research Computing cluster. The input file and Bash script are for illustration purposes and will look different after the user modifies the input file.

### Author info ###
Written by Maxwell Pietsch

maxwell.pietsch@gmail.com

http://maxwell9.myweb.usf.edu